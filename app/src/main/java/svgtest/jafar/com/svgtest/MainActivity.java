package svgtest.jafar.com.svgtest;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import svgtest.jafar.com.svgtest.ui.AnimatedSVGView;

public class MainActivity extends AppCompatActivity {

    private AnimatedSVGView svgView;
    private int index = -1;
    private int flag = 0;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        svgView = (AnimatedSVGView) findViewById(R.id.animated_svg_view);

        svgView.postDelayed(new Runnable() {

            @Override public void run() {
                svgView.start();
            }
        }, 500);

        svgView.setOnClickListener(new View.OnClickListener() {

            @Override public void onClick(View v) {
                if (svgView.getState() == AnimatedSVGView.STATE_FINISHED) {
                    flag++;
                    switch (flag) {
                        case 1:
                            svgView.setFillColor(Color.BLACK);
                            break;
                        case 2:
                            svgView.setFillColor(Color.YELLOW);
                            break;
                        case 3:
                            svgView.setFillColor(Color.BLUE);
                            break;
                        case 4:
                            svgView.setFillColor(Color.CYAN);
                            break;
                        case 5:
                            svgView.setFillColor(Color.GREEN);
                            break;
                        case 6:
                            svgView.setFillColor(Color.RED);
                            break;
                        default:
                            flag = 1;
                            svgView.setFillColor(Color.BLACK);
                            break;
                    }

                    svgView.start();
                }
            }
        });

        svgView.setOnStateChangeListener(new AnimatedSVGView.OnStateChangeListener() {

            @Override public void onStateChange(int state) {
                if (state == AnimatedSVGView.STATE_TRACE_STARTED) {
                    findViewById(R.id.btn_previous).setEnabled(false);
                    findViewById(R.id.btn_next).setEnabled(false);
                } else if (state == AnimatedSVGView.STATE_FINISHED) {
                    findViewById(R.id.btn_previous).setEnabled(index != -1);
                    findViewById(R.id.btn_next).setEnabled(true);
                    if (index == -1) index = 0; // first time
                }
            }
        });
    }

    public void onNext(View view) {
        if (++index >= SVG.values().length) index = 0;
        setSvg(SVG.values()[index]);
    }

    public void onPrevious(View view) {
        if (--index < 0) index = SVG.values().length - 1;
        setSvg(SVG.values()[index]);
    }

    private void setSvg(SVG svg) {
        svgView.setGlyphStrings(svg.glyphs);
        svgView.setFillColors(svg.colors);
        svgView.setViewportSize(svg.width, svg.height);
        svgView.setTraceResidueColor(0x32000000);
        svgView.setTraceColors(svg.colors);
        svgView.rebuildGlyphData();
        svgView.start();
    }

}
